import { Participant } from "src/app/Interfaces/summoner.interface";

export interface FeaturedGame {
    gameMode: string;
    gameLenght: number;
    mapId: number;
    gameType: string;
    bannedChampions: [],
    gameId: number;
    observers: {};
    gameQueueConfigId: number;
    gameStartTime: number;
    participants: Array<Participant>;
    platformId: string;
}

export interface FeaturedGameList {
    gameList: FeaturedGame[],
    clientRefreshInterval: number;

}