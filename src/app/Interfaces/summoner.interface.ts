export interface Summoner {
    accountId: string;
    id: string;
    name: string;
    profileIconId: number;
    puuid: string;
    revisionDate: number;
    summonerLevel: number;
}

export interface Participant {
    bot: boolean;
    championId: number;
    gameCustomizationObjects: [],
    perks: [],
    profileIconId: number;
    spell1Id: number;
    spell2Id: number;
    summonerId: string;
    summonerName: string;
    teamId: number;
    mastery?: number;
}

export interface ChampionMastery {
    championPointsUntilNextLevel: number;
    chestGranted: boolean;
    championId: number;
    lastPlayTime: number;
    championLevel: number;
    summonerId: string;
    championPoints: number;
    championPointsSinceLastLevel: number;
    tokensEarned: number;
}