import { NgModule} from '@angular/core';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';





@NgModule({
    exports: [MatToolbarModule, MatFormFieldModule, MatButtonModule, MatCardModule]
})

export class MaterialModule {

}