import { FeaturedGame, FeaturedGameList } from '../../Interfaces/match.interface';
import { Component, OnInit } from '@angular/core';
import { RiotAPiService } from 'src/app/services/riot-api-service';
import { Observable, tap } from 'rxjs';
import { ParticipantService } from 'src/app/services/participant.service';
import { ChampionMastery, Participant } from '../../Interfaces/summoner.interface';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  // featuredGames!: Observable<FeaturedGameList>;
  featuredGames!: FeaturedGameList;

  aramGame!: FeaturedGame;
  classicGame!: FeaturedGame;

  fullGame!:FeaturedGame
  constructor(private RiotAPiService: RiotAPiService, private ParticipantService: ParticipantService) { }

  ngOnInit(): void {
    this.RiotAPiService.getLiveGames().subscribe((match) => {
      this.aramGame = this.getGameByType('ARAM', match.gameList);
      this.classicGame = this.getGameByType('CLASSIC', match.gameList);

      
      [this.aramGame, this.classicGame].forEach((game) => {
        game.participants.forEach((participant: Participant, index)=> {
          this.ParticipantService.getMaestry(participant).subscribe(( championMastery: ChampionMastery) => {
            participant.mastery = championMastery.championLevel;
            game.participants[index] = participant;
          }
            
          );
        });
      })

    });
  }


  getGameLink(participants: any[]){
    return participants[0].summonerName;
  }

  getGameByType(type: string, gameList: FeaturedGame[]): FeaturedGame{
    return gameList.filter(game => game.gameMode.toLowerCase() === type.toLowerCase())[0];  
  };

  

}
