import { ChampionMastery, Participant } from "src/app/Interfaces/summoner.interface";
import { RiotAPiService } from "./riot-api-service";
import { Summoner } from "src/app/Interfaces/summoner.interface";
import { Observable, switchMap } from "rxjs";
import { Injectable } from "@angular/core";


@Injectable({
    providedIn: 'root'
})
export class ParticipantService {

    constructor(private RiotAPiService: RiotAPiService) {};

    
    getMaestry(participant: Participant): Observable<ChampionMastery> {
        const summonerMaestry$ = this.RiotAPiService.getSummoner(participant.summonerName).pipe(
            switchMap((summoner: Summoner) => this.RiotAPiService.getChampionMaestry(summoner.id, participant.championId) 
        ))

        return summonerMaestry$;

    }

}


