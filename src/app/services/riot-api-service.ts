import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FeaturedGame, FeaturedGameList } from 'src/app/Interfaces/match.interface';
import { ChampionMastery, Summoner } from 'src/app/Interfaces/summoner.interface';

// 'summoner' is a string to identify the method being used currently
// See note about rate-limiting in the README.
// Also see https://developer.riotgames.com/rate-limiting.html#method-headers

@Injectable({
    providedIn: 'root'
})

export class RiotAPiService {

    
    private apiUrl = '/lol';
    private apiKey = 'RGAPI-5dcf0a7f-507b-4190-ba4b-8362c5381fc0'
    constructor(private http: HttpClient) {};

    getSummoner(summonerName: string): Observable<Summoner> {
        return this.http.get<Summoner>(this.apiUrl + '/summoner/v4/summoners/by-name/' + summonerName.toLowerCase()  + '?api_key=' + this.apiKey)
    }

    getLiveGames() : Observable<FeaturedGameList> {
        return this.http.get<FeaturedGameList>(this.apiUrl + '/spectator/v4/featured-games' + '?api_key=' + this.apiKey)
        
    }

    getLiveGame(encryptedSummonerId: string): Observable<FeaturedGame>{
        return this.http.get<FeaturedGame>(this.apiUrl + '/spectator/v4/active-games/by-summoner/' + encryptedSummonerId + '?api_key=' + this.apiKey)
    }


    getChampionMaestry(id: string, championId: number): Observable<ChampionMastery> {
        return this.http.get<ChampionMastery>(this.apiUrl + '/champion-mastery/v4/champion-masteries/by-summoner/'+id +'/by-champion/' + championId + '?api_key=' + this.apiKey)
    }
}



