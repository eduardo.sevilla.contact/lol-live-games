import { Component, Input, OnInit } from '@angular/core';
import { FeaturedGame } from 'src/app/Interfaces/match.interface';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.scss']
})
export class MatchComponent implements OnInit {
  @Input()
  match!: FeaturedGame;
  constructor() { }

  ngOnInit(): void {
  }

  getBlueTeam(participants: any[]) {
    return participants.filter(participant => participant.teamId === 200);
}

getRedTeam(participants: any[]) {
  return participants.filter(participant => participant.teamId === 100);
}

}
